/* Application Sripts */
'use strict';

var app = angular.module('WorldClock', []);

app.controller('WorldClockController', ['$scope', '$interval', function ($scope, $interval) {

  $scope.MIN_VALUE = -23;
  $scope.MAX_VALUE = 23;
  $scope.torontoTime = new Date();
  $scope.londonTimeDiff = 0;
  $scope.sydneyTimeDiff = 0;
  $scope.newTorontoTime;


  $scope.setTorontoTime = function () {
    console.log($scope.newTorontoTime);
    $scope.torontoTime = $scope.newTorontoTime;
    console.log($scope.torontoTime);
  }

  function updateMyTime() {
    $scope.torontoTime = new Date();
    $scope.londonTime = new Date(new Date().setHours(new Date().getHours() + $scope.londonTimeDiff));
    $scope.sydneyTime = new Date(new Date().setHours(new Date().getHours() + $scope.sydneyTimeDiff));
  }

  $interval(updateMyTime, 5000);

  $scope.differenceForLondon = function () {
    if ($scope.londonTimeDiff >= -23 && $scope.londonTimeDiff <= 23) {
      $scope.londonTime = new Date();
      $scope.londonTimeDiff = parseInt($scope.londonTimeDiff);
      $scope.londonTime = new Date(new Date().setHours(new Date().getHours() + $scope.londonTimeDiff));
    }
  }

  $scope.differenceForSydney = function () {
    if ($scope.sydneyTimeDiff >= -23 && $scope.sydneyTimeDiff <= 23) {
      $scope.sydneyTime = new Date();
      $scope.sydneyTimeDiff = parseInt($scope.sydneyTimeDiff);
      $scope.sydneyTime = new Date(new Date().setHours(new Date().getHours() + $scope.sydneyTimeDiff));
    }
  }

}]);